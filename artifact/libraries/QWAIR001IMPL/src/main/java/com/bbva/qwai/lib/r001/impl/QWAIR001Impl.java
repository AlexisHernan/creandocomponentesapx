package com.bbva.qwai.lib.r001.impl;


import com.bbva.qwai.dto.customers.CustomerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.bbva.apx.exception.db.IncorrectResultSizeException;
import com.bbva.apx.exception.db.NoResultException;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import java.util.HashMap;


public class QWAIR001Impl extends QWAIR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001Impl.class);


	@Override
	public List<CustomerDTO> execute() {
			List<CustomerDTO> listDTO = new ArrayList<>();

			CustomerDTO cust1 = new CustomerDTO();
			CustomerDTO cust2 = new CustomerDTO();

			Date date = new Date();

			// First cust1
			cust1.setCustomerId("00001");
			cust1.setFirstName("Jhon");
			cust1.setLastName("Freeman");
			cust1.setNationality("EN");
			cust1.setPersonalTitle("Sr");
			cust1.setGenderId("Male");
			cust1.setIdentityDocumentType("1");
			cust1.setIdentityDocumentNumber("12345678N");
			cust1.setBirthDate(date );
			cust1.setMaritalStatus("Single");

			// Second cust2
			cust2.setCustomerId("00002");
			cust2.setFirstName("Juan");
			cust2.setLastName("Perez");
			cust2.setNationality("ES");
			cust2.setPersonalTitle("Sr");
			cust2.setGenderId("Male");
			cust2.setIdentityDocumentType("1");
			cust2.setIdentityDocumentNumber("98765432N");
			cust2.setBirthDate(date);
			cust2.setMaritalStatus("Single");

			listDTO.add(cust1);
			listDTO.add(cust2);

			return listDTO;

	}

	@Override
	public List<CustomerDTO> executeDocument(String identityDocumentType, String identityDocumentNumber) {
		List<CustomerDTO> listDTO = new ArrayList<>();
		if (StringUtils.isBlank(identityDocumentType) || StringUtils.isBlank(identityDocumentNumber)){
			this.addAdvice("QWAI00000001");
		} else {
			CustomerDTO customerDTO = null;
			String type = this.applicationConfigurationService.getDefaultProperty("type.find", "hardcode");
			LOGGER.error("type.find {}", type);
			switch (type) {
				case "jdbc":
					customerDTO = this.getCustomerDTOFromJDBC(identityDocumentType, identityDocumentNumber);
					break;
				case "intapiconnector":
					customerDTO = this.getCustomerDTOFromApi(identityDocumentType, identityDocumentNumber);
					break;
				case "intback":
					customerDTO = this.getCustomerDTOFromBackEnd(identityDocumentType, identityDocumentNumber);
					break;
				default:
					customerDTO = this.getCustomerDTOHardCode(identityDocumentType, identityDocumentNumber);
			}
			if (customerDTO != null) {
				listDTO.add(customerDTO);
			}
		}
		return listDTO;
	}

	private CustomerDTO getCustomerDTOFromJDBC(String identityDocumentType, String identityDocumentNumber) {
		LOGGER.info("getCustomerDTOFromJDBC({}, {})", identityDocumentType, identityDocumentNumber);
		CustomerDTO customerDTO = null;
		Map<String, Object> mapCustomerDTO;
		try {
			mapCustomerDTO = this.jdbcUtils.queryForMap("select.customer.by.document", identityDocumentType, identityDocumentNumber);
		} catch(IncorrectResultSizeException ex) {
			LOGGER.error(ex.toString());
			mapCustomerDTO = null;
		} catch(NoResultException ex) {
			mapCustomerDTO = null;
		}

		if (mapCustomerDTO != null && !mapCustomerDTO.isEmpty()) {
			customerDTO = new CustomerDTO();
			customerDTO.setCustomerId((String) mapCustomerDTO.get("customerId"));
			customerDTO.setFirstName((String) mapCustomerDTO.get("firstName"));
			customerDTO.setLastName((String) mapCustomerDTO.get("lastName"));
			customerDTO.setNationality((String) mapCustomerDTO.get("nationality"));
			customerDTO.setPersonalTitle((String) mapCustomerDTO.get("personalTitle"));
			customerDTO.setGenderId((String) mapCustomerDTO.get("genderId"));
			customerDTO.setIdentityDocumentType((String) mapCustomerDTO.get("identityDocumentType"));
			customerDTO.setIdentityDocumentNumber((String) mapCustomerDTO.get("identityDocumentNumber"));
			customerDTO.setBirthDate((Date) mapCustomerDTO.get("birthDate"));
			customerDTO.setMaritalStatus((String) mapCustomerDTO.get("maritalStatus"));
		}
		return customerDTO;
	}

	/*
	 * Get customer from ApiConnector
	 */
	private CustomerDTO getCustomerDTOFromApi(String identityDocumentType, String identityDocumentNumber) {
		// api.connector.customer.url = http://localhost:3000/customer
		LOGGER.info("getCustomerDTOFromApi({}, {})", identityDocumentType, identityDocumentNumber);
		ResponseEntity<CustomerDTO> customerDTOEntity = this.internalApiConnector.getForEntity("customer", CustomerDTO.class, identityDocumentType, identityDocumentNumber);
		return customerDTOEntity.getBody();
	}

	/*
	 * Get customer from interbackend
	 */
	private CustomerDTO getCustomerDTOFromBackEnd(String identityDocumentType, String identityDocumentNumber) {
		LOGGER.info("getCustomerDTOFromBackEnd({}, {})", identityDocumentType, identityDocumentNumber);
		CustomerDTO customerDTO = null;
		Map<String, Object> paramsIn = new HashMap<>();
		paramsIn.put("identityDocumentType", identityDocumentType);
		paramsIn.put("identityDocumentNumber", identityDocumentNumber);
		Map<String, Object> customerDTOBackEnd = this.interBackendConnectionUtils.invoke("UUAAT001010101", paramsIn);
		if (customerDTOBackEnd != null && !customerDTOBackEnd.isEmpty()) {
			customerDTO = new CustomerDTO();
			customerDTO.setCustomerId((String) customerDTOBackEnd.get("customerId"));
			customerDTO.setFirstName((String) customerDTOBackEnd.get("firstName"));
			customerDTO.setLastName((String) customerDTOBackEnd.get("lastName"));
			customerDTO.setNationality((String) customerDTOBackEnd.get("nationality"));
			customerDTO.setPersonalTitle((String) customerDTOBackEnd.get("personalTitle"));
			customerDTO.setGenderId((String) customerDTOBackEnd.get("genderId"));
			customerDTO.setIdentityDocumentType((String) customerDTOBackEnd.get("identityDocumentType"));
			customerDTO.setIdentityDocumentNumber((String) customerDTOBackEnd.get("identityDocumentNumber"));
			customerDTO.setBirthDate((Date) customerDTOBackEnd.get("birthDate"));
			customerDTO.setMaritalStatus((String) customerDTOBackEnd.get("maritalStatus"));
		}
		return customerDTO;
	}

	private CustomerDTO getCustomerDTOHardCode(String identityDocumentType, String identityDocumentNumber) {
		LOGGER.info("getCustomerDTOHardcode({}, {}", identityDocumentType, identityDocumentNumber);
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setCustomerId("00003");
		customerDTO.setFirstName("Carlos");
		customerDTO.setLastName("Martín");
		customerDTO.setNationality("ES");
		customerDTO.setPersonalTitle("Sr");
		customerDTO.setGenderId("Male");
		customerDTO.setIdentityDocumentType(identityDocumentType);
		customerDTO.setIdentityDocumentNumber(identityDocumentNumber);
		customerDTO.setBirthDate(new Date());
		customerDTO.setMaritalStatus("Single");
		return customerDTO;
	}
}
