package com.bbva.qwai.lib.r001.impl;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.elara.utility.api.connector.APIConnector;
import com.bbva.elara.utility.api.connector.APIConnectorBuilder;
import com.bbva.elara.utility.interbackend.InterBackendConnectionUtils;
import com.bbva.elara.utility.interbackend.cics.InterBackendCicsUtils;
import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.qwai.lib.r001.QWAIR001;

/**
 * This class automatically defines the libraries and utilities that it will use.
 */
public abstract class QWAIR001Abstract extends AbstractLibrary implements QWAIR001 {

	protected ApplicationConfigurationService applicationConfigurationService;

	protected InterBackendCicsUtils interBackendCicsUtils;

	protected JdbcUtils jdbcUtils;

	protected APIConnector internalApiConnector;

	protected APIConnectorBuilder apiConnectorBuilder;

	protected InterBackendConnectionUtils interBackendConnectionUtils;


	/**
	* @param applicationConfigurationService the this.applicationConfigurationService to set
	*/
	public void setApplicationConfigurationService(ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}

	/**
	* @param interBackendCicsUtils the this.interBackendCicsUtils to set
	*/
	public void setInterBackendCicsUtils(InterBackendCicsUtils interBackendCicsUtils) {
		this.interBackendCicsUtils = interBackendCicsUtils;
	}

	/**
	* @param jdbcUtils the this.jdbcUtils to set
	*/
	public void setJdbcUtils(JdbcUtils jdbcUtils) {
		this.jdbcUtils = jdbcUtils;
	}

	/**
	* @param internalApiConnector the this.internalApiConnector to set
	*/
	public void setInternalApiConnector(APIConnector internalApiConnector) {
		this.internalApiConnector = internalApiConnector;
	}

	/**
	* @param apiConnectorBuilder the this.apiConnectorBuilder to set
	*/
	public void setApiConnectorBuilder(APIConnectorBuilder apiConnectorBuilder) {
		this.apiConnectorBuilder = apiConnectorBuilder;
	}

	/**
	* @param interBackendConnectionUtils the this.interBackendConnectionUtils to set
	*/
	public void setInterBackendConnectionUtils(InterBackendConnectionUtils interBackendConnectionUtils) {
		this.interBackendConnectionUtils = interBackendConnectionUtils;
	}

}