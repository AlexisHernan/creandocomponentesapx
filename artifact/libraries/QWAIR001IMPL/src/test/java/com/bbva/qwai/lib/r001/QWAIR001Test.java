package com.bbva.qwai.lib.r001;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.bbva.elara.utility.api.connector.APIConnector;
import com.bbva.elara.utility.interbackend.InterBackendConnectionUtils;
import com.bbva.elara.utility.jdbc.JdbcUtils;
import org.mockito.Spy;
import com.bbva.qwai.dto.customers.CustomerDTO;
import java.util.List;
import org.junit.Assert;
import org.mockito.Mockito;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import com.bbva.apx.exception.db.NoResultException;
import org.springframework.http.ResponseEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/QWAIR001-app.xml",
		"classpath:/META-INF/spring/QWAIR001-app-test.xml",
		"classpath:/META-INF/spring/QWAIR001-arc.xml",
		"classpath:/META-INF/spring/QWAIR001-arc-test.xml" })
public class QWAIR001Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001Test.class);

	@Spy
	private Context context;

	@Resource(name = "qwaiR001")
	private QWAIR001 qwaiR001;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Resource(name = "jdbcUtils")
	private JdbcUtils jdbcUtils;

	@Resource(name = "internalApiConnector")
	private APIConnector internalApiConnector;

	@Resource(name = "interBackendConnectionUtils")
	private InterBackendConnectionUtils interBackendConnectionUtils;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		context = new Context();
		ThreadContext.set(context);
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.qwaiR001;
		if(this.qwaiR001 instanceof Advised){
			Advised advised = (Advised) this.qwaiR001;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeListTest(){
		LOGGER.info("Executing the test...");
		List<CustomerDTO> listCustomer = qwaiR001.execute();
		Assert.assertTrue(listCustomer.size() > 0);
	}

	@Test
	public void executeDocumentNotFoundTest(){
		List<CustomerDTO> listCustomer = qwaiR001.executeDocument("", "");
		Assert.assertEquals(0, listCustomer.size());
		Assert.assertEquals(1, context.getAdviceList().size());
		Assert.assertEquals("QWAI00000001", context.getAdviceList().get(0).getCode());
	}

	@Test
	public void executeDocumentJDBCTest(){
		Mockito.when(applicationConfigurationService.getDefaultProperty("type.find", "hardcode")).thenReturn("jdbc");
		Mockito.when(jdbcUtils.queryForMap("select.customer.by.document", "DNI", "12341234N")).thenReturn(getCustomerDTOMap());
		List<CustomerDTO> listCustomer = qwaiR001.executeDocument("DNI", "12341234N");
		Assert.assertEquals(1, listCustomer.size());
	}

	private Map<String, Object> getCustomerDTOMap() {
		Map<String, Object> mapCustomerDTO = new HashMap<>();
		mapCustomerDTO.put("customerId", "12345");
		mapCustomerDTO.put("firstName", "First Name Test");
		mapCustomerDTO.put("lastName", "Last Name Test");
		mapCustomerDTO.put("nationality", "ES");
		mapCustomerDTO.put("personalTitle", "Sr");
		mapCustomerDTO.put("genderId", "Male");
		mapCustomerDTO.put("identityDocumentType", "DNI");
		mapCustomerDTO.put("identityDocumentNumber", "12341234N");
		mapCustomerDTO.put("birthDate", new Date());
		mapCustomerDTO.put("maritalStatus", "Single");
		return mapCustomerDTO;
	}

	@Test
	public void executeDocumentJDBCExceptionTest(){
		Mockito.when(applicationConfigurationService.getDefaultProperty("type.find", "hardcode")).thenReturn("jdbc");
		Mockito.when(jdbcUtils.queryForMap("select.customer.by.document", "DNI", "66666666A")).thenThrow(NoResultException.class);
		List<CustomerDTO> listCustomer = qwaiR001.executeDocument("DNI", "66666666A");
		Assert.assertEquals(0, listCustomer.size());
	}

	@Test
	public void executeDocumentIntApiConnectorTest(){
		Mockito.when(applicationConfigurationService.getDefaultProperty("type.find", "hardcode")).thenReturn("intapiconnector");
		ResponseEntity responseEntity = Mockito.mock(ResponseEntity.class);
		Mockito.when(responseEntity.getBody()).thenReturn(getCustomerDTO());
		Mockito.when(internalApiConnector.getForEntity("customer", CustomerDTO.class, "DNI", "12341234N")).thenReturn(responseEntity);
		List<CustomerDTO> listCustomer = qwaiR001.executeDocument("DNI", "12341234N");
		Assert.assertEquals(1, listCustomer.size());
	}

	/*
	 * Method getCustomerDTO
	 */
	private CustomerDTO getCustomerDTO() {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setCustomerId("12345");
		customerDTO.setFirstName("First Name Test");
		customerDTO.setLastName("Last Name Test");
		customerDTO.setNationality("ES");
		customerDTO.setPersonalTitle("Sr");
		customerDTO.setGenderId("Male");
		customerDTO.setIdentityDocumentType("DNI");
		customerDTO.setIdentityDocumentNumber("12341234N");
		customerDTO.setBirthDate(new Date());
		customerDTO.setMaritalStatus("Single");
		return customerDTO;
	}

	@Test
	public void executeDocumentImsBackendTest(){
		Mockito.when(applicationConfigurationService.getDefaultProperty("type.find", "hardcode")).thenReturn("intback");

		HashMap<String, Object> paramsIn = new HashMap<>();
		paramsIn.put("identityDocumentType", "DNI");
		paramsIn.put("identityDocumentNumber", "12341234N");

		Mockito.when(interBackendConnectionUtils.invoke("UUAAT001010101", paramsIn)).thenReturn(getCustomerDTOMap());
		List<CustomerDTO> listCustomer = qwaiR001.executeDocument("DNI", "12341234N");
		Assert.assertEquals(1, listCustomer.size());
	}

	@Test
	public void executeDocumentHardCodeTest(){
		Mockito.when(applicationConfigurationService.getDefaultProperty("type.find", "hardcode")).thenReturn("hardcode");
		List<CustomerDTO> listCustomer = qwaiR001.executeDocument("DNI", "12341234N");
		Assert.assertEquals(1, listCustomer.size());
	}


}
