package com.bbva.qwai.lib.r001;


import com.bbva.qwai.dto.customers.CustomerDTO;
import java.util.List;

public interface QWAIR001 {

	List<CustomerDTO> execute();

	List<CustomerDTO> executeDocument(String identityDocumentType, String identityDocumentNumber);

}
