package com.bbva.qwai;

import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.TransactionParameter;
import com.bbva.elara.domain.transaction.request.TransactionRequest;
import com.bbva.elara.domain.transaction.request.body.CommonRequestBody;
import com.bbva.elara.domain.transaction.request.header.CommonRequestHeader;
import com.bbva.elara.test.osgi.DummyBundleContext;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;

import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test for transaction QWAIT00101MXTransaction
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/elara-test.xml",
		"classpath:/META-INF/spring/QWAIT00101MXTest.xml" })
public class QWAIT00101MXTransactionTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00101MXTransactionTest.class);

	@Autowired
	private QWAIT00101MXTransaction transaction;

	@Resource(name = "dummyBundleContext")
	private DummyBundleContext bundleContext;

	@Resource(name = "qwaiR001")
	private QWAIR001 qwaiR001;

	@Mock
	private CommonRequestHeader header;

	@Mock
	private TransactionRequest transactionRequest;


	@Before
	public void initializeClass() throws Exception {
		// Initializing mocks
		MockitoAnnotations.initMocks(this);
		// Start BundleContext
		this.transaction.start(bundleContext);
		// Setting Context
		this.transaction.setContext(new Context());
		// Set Body
		CommonRequestBody commonRequestBody = new CommonRequestBody();
		commonRequestBody.setTransactionParameters(new ArrayList<>());
		this.transactionRequest.setBody(commonRequestBody);
		// Set Header Mock
		this.transactionRequest.setHeader(header);
		// Set TransactionRequest
		this.transaction.getContext().setTransactionRequest(transactionRequest);
	}

	@Test
	public void testNotNull(){
	    // Example to Mock the Header
		// Mockito.doReturn("ES").when(header).getHeaderParameter(RequestHeaderParamsName.COUNTRYCODE);
		Assert.assertNotNull(this.transaction);
		this.transaction.execute();
	}

	// Add Parameter to Transaction
	private void addParameter(final String parameter, final Object value) {
		final TransactionParameter tParameter = new TransactionParameter(parameter, value);
		transaction.getContext().getParameterList().put(parameter, tParameter);
	}

	// Get Parameter from Transaction
	private Object getParameter(final String parameter) {
		final TransactionParameter param = transaction.getContext().getParameterList().get(parameter);
		return param != null ? param.getValue() : null;
	}


	@Test
	public void testExecuteReturnEmpty(){
		Mockito.when(qwaiR001.execute()).thenReturn(getListDTO(0));
		Assert.assertNotNull(this.transaction);
		this.transaction.execute();
		Assert.assertEquals(Severity.WARN, this.transaction.getContext().getSeverity());
	}

	@Test
	public void testExecuteReturnList(){
		Mockito.when(qwaiR001.execute()).thenReturn(getListDTO(2));
		Assert.assertNotNull(this.transaction);
		this.transaction.execute();
		Assert.assertEquals(Severity.OK, this.transaction.getContext().getSeverity());
		List<CustomerDTO> listDTO = (List<CustomerDTO>)this.getParameter("EntityList");
		Assert.assertEquals(2, listDTO.size());
	}

	@Test
	public void testExecuteByDocumentReturnList(){
		String identityDocumentType = "DNI";
		String identityDocumentNumber = "12341234N";

		addParameter("identityDocumentType", identityDocumentType);
		addParameter("identityDocumentNumber", identityDocumentNumber);

		Mockito.when(qwaiR001.executeDocument(identityDocumentType, identityDocumentNumber)).thenReturn(getListDTO(1));

		Assert.assertNotNull(this.transaction);
		this.transaction.execute();
		Assert.assertEquals(Severity.OK, this.transaction.getContext().getSeverity());
		List<CustomerDTO> listDTO = (List<CustomerDTO>)this.getParameter("EntityList");
		Assert.assertEquals(1, listDTO.size());
	}

	private List<CustomerDTO> getListDTO(int intDTO) {
		List<CustomerDTO> listDTO = new ArrayList<>();
		for (int i = 0; i < intDTO; i++) {
			CustomerDTO customerDTO = new CustomerDTO();
			listDTO.add(customerDTO);
		}
		return listDTO;
	}
}
