package com.bbva.qwai;

import com.bbva.elara.transaction.AbstractTransaction;
import com.bbva.qwai.dto.customers.CustomerDTO;
import java.util.List;

/**
 * In this class, the input and output data is defined automatically through the setters and getters.
 */
public abstract class AbstractQWAIT00101MXTransaction extends AbstractTransaction {

	public AbstractQWAIT00101MXTransaction(){
	}


	/**
	 * Return value for input parameter identityDocumentType
	 */
	protected String getIdentitydocumenttype(){
		return (String)this.getParameter("identityDocumentType");
	}

	/**
	 * Return value for input parameter identityDocumentNumber
	 */
	protected String getIdentitydocumentnumber(){
		return (String)this.getParameter("identityDocumentNumber");
	}

	/**
	 * Set value for List<CustomerDTO> output parameter EntityList
	 */
	protected void setEntitylist(final List<CustomerDTO> field){
		this.addParameter("EntityList", field);
	}
}
